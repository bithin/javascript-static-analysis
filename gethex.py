import re
import operator


def readhex(filename):
    """
    Reads the hexadecimal strings from the testfile
    """
    words = []
    count = 0
    hexstring = ''
    hexlist = []
    hexs = ''
    hstring = ''
    hexDict = {}
    sortedhexList = []

    with open(filename, 'r') as f:
        t = f.read()
    
    for i in t:
        if i == '\\':
            words.append('\\')
        elif re.match("^[a-zA-Z0-9,\"]$", i):
            words.append(i)
        else:
            continue

    while count <= len(words)-3:
        if words[count] == '\\':
            hexstring += words[count]
            count = count+1
            hexstring += words[count]
            count = count+1 
            hexstring += words[count]
            count = count+1
            hexstring += words[count]
            if str(hexstring) != '':
                hexs += str(hexstring)
            hexstring = ''
        else:
            if hexs != '':
                hexs = hexs.replace("\\x", '')
                hstring = str(hexs.decode('hex'))
		try:
			hexDict[hstring] += 1
		except(KeyError):
			hexDict[hstring] = 1
            hexs = ''
        
        count = count + 1

    sorted_hex = sorted(hexDict.items(), key=operator.itemgetter(1), reverse=True)
    for i in sorted_hex:
        sortedhexList.append([i[0], i[1]])

    return sortedhexList


def dictCompare(initDict, fileList):
    """
    Compares the words in the dictionary with
    the words given in the testFile
    """
    detech = False
    score = 0 
    threshold = calculateThreshold(initDict)
    listlen = int(0.6 * len(fileList))
    if listlen < 1:
        return False

    for k in range(listlen):
        for keyword in initDict.keys():
            if fileList[k][0] == keyword:
                score = score + int(initDict[keyword])
                break;
   
    print "Threshold: %d, Score: %d" % (threshold, score)

    if score >= threshold:
        detech = True

    return detech


def calculateThreshold(initDist):
    """
    Calculates the threshold of the sample dictionary
    """
    count = 0
    score = 0
    listlen = 0.4 * len(initDist)
    for key in initDist.keys():
        if count <= listlen:
            score = score + int(initDist[key])
        else:
            break
        count = count + 1
    return score 


def readMalSample(malwareSample):
    """
    Calculates the word frequency list of the 
    sample malware
    """
    initDict = {}
    with open(malwareSample, 'r') as f:
        sample = f.read().split("\n")
        for line in sample:
            if line != '':
                i = line.split(":")
                initDict[i[0]] = i[1] 
    return initDict


if __name__ == "__main__":
    """
    Main function
    """
    hexList  = readhex('iframe.txt')
    initDict = readMalSample('JSMalware.txt')
    malDict  = dictCompare(initDict,hexList)
    if malDict:
	print 'This sample contains suspicious code. Suspicious samples are given below'
	print malDict
    else:
	print 'Sample appears to be clean.'
